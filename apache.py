from fabric import Connection
from invoke import Responder
from invoke.exceptions import UnexpectedExit


class Apache:

    def __init__(self, host, username, password) -> None:
        self.connection = Connection(host=host, user=username, connect_kwargs={ 'password': password })
        self.sudopass = Responder(
            pattern=r'\[sudo\] password:',
            response=f'{password}\n',
        )
    
    def install(self):
        try:
            self.connection.run('sudo apt install -y apache2', pty = True)
        except Exception as e:
            # ??
            raise Exception("ca va mal...") from e
    
    def uninstall(self):
        self.connection.run('sudo apt remove -y apache2*', pty=True)

    def is_installed(self) -> bool:
        try: 
            self.connection.run('apt list --installed 2> /dev/null | grep apache2/')
        except UnexpectedExit:
            return False
        else:
            return True

    def start(self):
        self.connection.run('sudo service apache2 start', pty=True)




# a = Apache("vps-3f8dfeab.vps.ovh.net", "andre", "")
# a.uninstall()